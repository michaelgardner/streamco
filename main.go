package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"

	"github.com/labstack/echo"
)

func main() {
	e := echo.New()
	// Unnecessary for virtual host env.
	//domain := os.Getenv("STREAMCO_DOMAIN")
	port := os.Getenv("STREAMCO_PORT")

	Routes(e)

	log.Printf("Running StreamCo Webservice :%s\n.", port)
	e.Run(":" + port)
}

func Routes(e *echo.Echo) {
	e.Post("/", Shows)
}

func Shows(c *echo.Context) error {
	payload := Payload{}
	// Read the JSON Body content into a payload
	err := json.NewDecoder(c.Request().Body).Decode(&payload)
	if err != nil {
		errMsg := ErrorMessage{
			Error: "Could not decode request: JSON parsing failed",
		}
		return c.JSON(http.StatusBadRequest, errMsg)
	}
	// Process Payload and Fetch DRM == true Show Images.
	imageResponse := payload.ProcessPayload()
	return c.JSON(http.StatusOK, &imageResponse)
}
