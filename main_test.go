package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/labstack/echo"
)

func TestShow(t *testing.T) {
	// Setting up these for use in comparison tests later.
	// See model.go
	expectR := ImageResponse{}
	actualR := ImageResponse{}
	// JSON data expected to be sent to our api
	// Read from file.
	requestJSON, err := ioutil.ReadFile("request.json")
	if err != nil {
		t.Error("Error reading request.json file.\n")
	}

	requestJSONReader := bytes.NewReader(requestJSON)

	// Prepare a Test Server.
	e := echo.New()
	Routes(e)
	testServer := httptest.NewServer(e.Router())
	defer testServer.Close()

	// Create a request.
	request, err := http.NewRequest("POST", testServer.URL, requestJSONReader)
	if err != nil {
		t.Errorf("Error creating request: %s\n", err.Error())
	}

	// Actually make the http request.
	client := http.Client{}
	response, err := client.Do(request)
	if err != nil {
		t.Fatalf("Error making http request: %s\n", err.Error())
	}

	// --- Testing for Status Code --- //

	// Basic status code test
	if response.StatusCode != http.StatusOK {
		t.Error("Incorrect status code received.")
	}

	// ---- Testing for correct content --- //

	// JSON data expected to be sent in response by our api
	// Read from file
	responseJSON, err := ioutil.ReadFile("response.json")
	if err != nil {
		t.Error("Error reading response.json file.\n")
	}

	// Prepare actual and expected ImageResponse.Image slices
	err = json.Unmarshal(responseJSON, &expectR)
	if err != nil {
		t.Error("Error unmarshalling responseJSON")
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		t.Error("Error reading response.Body")
	}
	err = json.Unmarshal(body, &actualR)
	if err != nil {
		t.Errorf("Error unmarshalling response.body: %v\n, error: %s", body, err.Error())
	}
	// Compare Actually Received Images to Expected Received Images
	// Both are of type ImageResponse -> model.go
	if !reflect.DeepEqual(actualR.Images, expectR.Images) {
		t.Error("Actual Images output did not equal the expected Images output.")
	}

}

func TestShowFail(t *testing.T) {
	// JSON data expected to be sent to our api
	// Read from file.
	badRequestJSON, err := ioutil.ReadFile("badRequest.json")
	if err != nil {
		t.Error("Error reading request.json file.\n")
	}
	badRequestReader := bytes.NewReader(badRequestJSON)

	// Prepare a Test Server.
	e := echo.New()
	Routes(e)
	testServer := httptest.NewServer(e.Router())
	defer testServer.Close()

	// Create a request.
	badRequest, err := http.NewRequest("POST", testServer.URL, badRequestReader)
	if err != nil {
		t.Errorf("Error creating request: %s\n", err.Error())
	}

	// Actually make the http request.
	client := http.Client{}
	response, err := client.Do(badRequest)
	if err != nil {
		t.Fatalf("Error making http request: %s\n", err.Error())
	}

	// --- Testing For Correct HTTP Status Code --- //

	// Basic status code test
	if response.StatusCode != http.StatusBadRequest {
		t.Errorf("Incorrect status code received. Got: %d\n", response.StatusCode)
	}

	// ---- Testing for Correct Error Response ---- //

	expectedErrMsg := ErrorMessage{
		Error: "Could not decode request: JSON parsing failed",
	}
	// Prepare actual and expected ImageResponse.Image slices
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		t.Error("Error reading response.Body")
	}
	actualErrMsg := ErrorMessage{}
	err = json.Unmarshal(body, &actualErrMsg)
	if err != nil {
		t.Errorf("Error unmarshalling response.body from errMsg: %v\n, error: %s", body, err.Error())
	}

	// Compare Actually Received Error to Expected Received Error
	if !reflect.DeepEqual(actualErrMsg, expectedErrMsg) {
		t.Errorf("Actual Error output did not equal the expected Error output.: %s\n", actualErrMsg.Error)
	}

}
