package main

type Payload struct {
	Show         []Show `json:"Payload"`
	Skip         int
	Take         int
	TotalRecords int
}

func (payload *Payload) ProcessPayload() *ImageResponse {
	imageResponse := ImageResponse{}
	showImage := ShowImage{}

	for i := range payload.Show {
		if payload.Show[i].DRM == true && payload.Show[i].EpisodeCount > 0 {
			showImage.Image = payload.Show[i].Image.ShowImage
			showImage.Slug = payload.Show[i].Slug
			showImage.Title = payload.Show[i].Title
			imageResponse.Images = append(imageResponse.Images, showImage)
		}
	}
	return &imageResponse
}

type Show struct {
	Country      string
	Description  string
	DRM          bool
	EpisodeCount int
	Genre        string
	Image        Image
	Language     string
	NextEpisode  Episode
	PrimaryColor string
	Seasons      []Season
	Slug         string
	Title        string
	TvChannel    string
}

type Image struct {
	ShowImage string
}

type Episode struct {
	Channel     string
	ChannelLogo string
	Date        string
	HTML        string
	URL         string
}

type Season struct {
	Slug string
}

// Modeling the data we return from the request

type ImageResponse struct {
	Images []ShowImage `json:"response"`
}

type ShowImage struct {
	Image string
	Slug  string
	Title string
}

// Error Message
type ErrorMessage struct {
	Error string `json:"error"`
}
