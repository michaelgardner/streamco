# Welcome to The StreamCo JSON API Golang Application

This application is a part of a code test, instructions found [here](https://streamco-coding-challenge.herokuapp.com). It is a Go language web server json api that has one end point only.  

The application takes a json data request containing a list of shows and responds with the images, slugs and titles of the shows that have a DRM equal to true.

An HTTP 'POST' to "http://<domain>:<port>/" with a body Content-Type of "application/json" will result in a reply of Content-Type "application/json". 
Expected JSON data is found in the file 'request.json' and the expected response the application will reply with is found in the file 'response.json'.


## Dependencies & Setup
In order to compile this application you will need to install the echo micro framework using the following command:

    go get github.com/labstack/echo

After downloading the echo framework you will need to set two environment variables: 

    "STREAMCO_DOMAIN" & "STREAMCO_PORT"

For example, in your ~/.bashrc file:

    export STREAMCO_PORT="8080"  # Do not put the colon ":" in the port.

To build the application, you will need to have Go installed on your computer. Download Go for your computer [here](https://golang.org/dl/).

This application was developed using Go 1.5.1 on a Mac ("darwin/amd64"). Help for installing Go can be found [here](https://golang.org/doc/install).

## Hosting

This application is (currently) being hosted on an Ubuntu 14.04 server at DigitalOcean. It shares its host with a couple of other go apps using a golang virtual host, the gist of which can be found [here](//https://gist.github.com/camoles/523dac8cc0fe40d52f66).  This negates the necessity to use Apache or Nginx for hosting multiple Go apps on a single virtual host.

I use supervisord to Daemonize the apps (so if the host is rebooted they recover and restart). An excellent tutorial on writing supervisord config files can be found [here](http://beego.me/docs/deploy/supervisor.md). You'll also need to set environment variables in your supervisord config file, so read [this](http://supervisord.org/subprocess.html#subprocess-environment) to learn how.

For any enquiries regarding this app, please email [mickgardner@gmail.com](mickgardner@gmail.com).

## Testing
All tests are in the main_test.go file.  To run the tests:

    go test

or

    go test -v

## Files

Golang Source code:

    main.go
    model.go

Files for testing JSON requests and responses

    badRequest.json
    request.json
    response.json

Build and Deploy scripts

    buildLinux.sh
    buildMac.sh
    deploy.sh

Supervisor Configuration File

    streamcoLinux.conf